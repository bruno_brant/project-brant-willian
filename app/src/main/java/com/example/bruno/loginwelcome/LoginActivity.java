package com.example.bruno.loginwelcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity{

    @BindView(R.id.editNome)
    EditText editNome;
    @BindView(R.id.editPassword)
    EditText editPassword;
    @BindView(R.id.login)
    Button login;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcomelogin);

        ButterKnife.bind(this);
    }
    @OnClick(R.id.login)
    void logar() {

        if (validarLogin(editNome.getText().toString(), editPassword.getText().toString())) {
            Intent intent = new Intent(this, MainActivity.class);

            intent.putExtra("nome", editNome.getText().toString());

            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.mensagem_erro, Toast.LENGTH_SHORT).show();
        }
    }
    public boolean validarLogin(String nome, String password) {
        return nome.equals("Brant") && password.equals("123");
    }
}
